extern crate amethyst;

use amethyst::{
    prelude::*,
    renderer::{DisplayConfig, DrawFlat2D, Pipeline, PosNormTex, RenderBundle, Stage, Event, KeyboardInput, VirtualKeyCode, WindowEvent},
    utils::application_root_dir,
};

use amethyst::renderer::{
    Camera, PngFormat, Projection, SpriteRender, SpriteSheet,
    SpriteSheetFormat, SpriteSheetHandle, Texture, TextureMetadata,
};

use amethyst::assets::{AssetStorage, Loader};
use amethyst::core::transform::Transform;
use amethyst::ecs::prelude::{Component, DenseVecStorage};
use amethyst::core::transform::bundle::TransformBundle;

pub const ARENA_HEIGHT: usize = 10;
pub const ARENA_WIDTH: usize = 10;
pub const CELL_WIDTH: f32 = 1.0;
pub const CELL_HEIGHT: f32 = 1.0;

struct GridGame;

impl SimpleState for GridGame {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        println!("Starting game!");
        let sprites = load_sprites(data.world);
        initialise_grid(data.world, sprites);
        initialise_camera(data.world);
    }

    fn handle_event(&mut self, _: StateData<'_, GameData<'_, '_>>, event: StateEvent) -> SimpleTrans {
        if let StateEvent::Window(event) = &event {
            match event {
                 Event::WindowEvent { event, .. } => match event {
                    WindowEvent::KeyboardInput {
                        input: KeyboardInput { virtual_keycode: Some(VirtualKeyCode::Escape), .. }, ..
                    } |
                    WindowEvent::CloseRequested => Trans::Quit,
                    _ => Trans::None,
                },
                _ => Trans::None,
            }
        } else {
            Trans::None
        }
    }

    fn update(&mut self, _: &mut StateData<'_, GameData<'_, '_>>) -> SimpleTrans {
        println!("Computing some more whoop-ass...");
        Trans::Quit
    }
}

fn load_sprites(world: &mut World) -> SpriteSheetHandle {
    let loader = world.read_resource::<Loader>();
    let texture_handle = {
        let texture_storage = world.read_resource::<AssetStorage<Texture>>();
        let path = format!(
            "{}/sprites/cells.png",
            application_root_dir()
        );
        loader.load(
            path,
            PngFormat,
            TextureMetadata::srgb_scale(),
            (),
            &texture_storage,
        )
    };
    let sprite_handle = {
        let sprite_sheet_store = world.read_resource::<AssetStorage<SpriteSheet>>();
        let path = format!(
            "{}/sprites/cells.ron",
            application_root_dir()
        );
        loader.load(
            path, // Here we load the associated ron file
            SpriteSheetFormat,
            texture_handle, // We pass it the handle of the texture we want it to use
            (),
            &sprite_sheet_store,
        )
    };
    sprite_handle
}

fn initialise_grid(world: &mut World, sprites: SpriteSheetHandle) {
    for y in 0..ARENA_HEIGHT {
        for x in 0..ARENA_WIDTH {
            let render = SpriteRender {
                sprite_sheet: sprites.clone(),
                sprite_number: 0,
            };
            let mut transform = Transform::default();
            transform.set_xyz(
                x as f32 * CELL_WIDTH + CELL_WIDTH * 0.5,
                y as f32 * CELL_HEIGHT + CELL_HEIGHT * 0.5,
                0.0
            );
            world.create_entity()
                .with(render)
                .with(transform)
                .build();
        }
    }
}

fn initialise_camera(world: &mut World) {
    let mut transform = Transform::default();
    transform.set_z(1.0);
    world
        .create_entity()
        .with(Camera::from(Projection::orthographic(
            0.0,
            ARENA_WIDTH as f32,
            0.0,
            ARENA_HEIGHT as f32,
        )))
        .with(transform)
        .build();
}

fn main() -> amethyst::Result<()> {
    amethyst::start_logger(Default::default());

    let path = format!(
        "{}/resources/display_config.ron",
        application_root_dir()
    );
    let config = DisplayConfig::load(&path);

    let pipe = Pipeline::build().with_stage(
        Stage::with_backbuffer()
            .clear_target([1.0, 1.0, 1.0, 1.0], 1.0)
            .with_pass(DrawFlat2D::new()),
    );

    let game_data =
        GameDataBuilder::default()
        .with_bundle(RenderBundle::new(pipe, Some(config)))?
        .with_bundle(TransformBundle::new())?;
    let mut game = Application::new("./", GridGame, game_data)?;

    game.run();

    Ok(())
}
